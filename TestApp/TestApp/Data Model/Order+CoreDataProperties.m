//
//  Order+CoreDataProperties.m
//  
//
//  Created by Nazar Gorobets on 6/6/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Order+CoreDataProperties.h"

@implementation Order (CoreDataProperties)

@dynamic nameUser;
@dynamic name;
@dynamic photo;
@dynamic count;
@dynamic price;

@end
