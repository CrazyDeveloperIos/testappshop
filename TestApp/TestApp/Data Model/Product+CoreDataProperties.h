//
//  Product+CoreDataProperties.h
//  
//
//  Created by Nazar Gorobets on 6/6/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product.h"

NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *countProduct;
@property (nullable, nonatomic, retain) NSString *nameProduct;
@property (nullable, nonatomic, retain) NSData *photoProduct;
@property (nullable, nonatomic, retain) NSNumber *priceProduct;
@property (nullable, nonatomic, retain) NSNumber *veganProduct;
@property (nullable, nonatomic, retain) NSNumber *organicProduct;
@property (nullable, nonatomic, retain) NSString *cathegory;

@end

NS_ASSUME_NONNULL_END
