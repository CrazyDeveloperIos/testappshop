//
//  DataManager.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/5/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property (strong, nonatomic) NSString *namePr;
@property (strong, nonatomic) NSData   *photoPr;
@property (assign, nonatomic) NSNumber *countPr;

- (instancetype) initWithServerSecurityResponse:(NSString*) name
                                      namePhoto:(NSString*) photo
                                   countProduct:(NSUInteger) count
                                   setCathegory:(NSString*) cathegory;

@end
