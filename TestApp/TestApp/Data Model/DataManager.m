//
//  DataManager.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/5/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "DataManager.h"
#import "Product+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

@implementation DataManager

- (instancetype) initWithServerSecurityResponse:(NSString*) name namePhoto:(NSString*) photo countProduct:(NSUInteger) count setCathegory:(NSString*)cathegory{
    
    self = [super init];
    
    if (self) {
        

        NSNumber* isveganProduct   = [NSNumber numberWithInteger:arc4random_uniform(2) == 0];
        NSNumber* isorganicProduct = [NSNumber numberWithInteger:arc4random_uniform(2) == 0];

        
        double priceProduct = rand() % 300;
        
        UIImage* imageProduct = [UIImage imageNamed:photo];
        NSData*  imageData    =  UIImagePNGRepresentation(imageProduct);
        
        self.namePr  = name;
        self.photoPr = imageData;
        self.countPr = [NSNumber numberWithInteger:count];
        
        Product *product = [Product MR_createEntity];
        
        product.cathegory    = cathegory;
        product.nameProduct  = self.namePr;
        product.photoProduct = self.photoPr;
        product.countProduct = self.countPr;
        product.priceProduct = [NSNumber numberWithDouble:priceProduct];
        product.veganProduct = isveganProduct;
        product.organicProduct = isorganicProduct;
        
        
        UIApplication *application   = [UIApplication sharedApplication];
        
        __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
            
            [application endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
            
        }];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
            
        } completion:^(BOOL success, NSError *error) {
            
            [application endBackgroundTask:bgTask];
             bgTask = UIBackgroundTaskInvalid;
            
        }];
        
        [self description];
    }
    
    return self;
    
}

- (NSString *) description{
    
    return [NSString stringWithFormat:@"name %@, photo %@, count %@", self.namePr, self.photoPr, self.countPr];
}

@end
