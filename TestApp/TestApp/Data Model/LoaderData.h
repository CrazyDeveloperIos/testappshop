//
//  LoaderData.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/5/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoaderData : NSObject

@property (strong, nonatomic) NSMutableArray *itemImages;
@property (strong, nonatomic) NSMutableArray *itemName;

- (void) loadingDataForDB;

@end
