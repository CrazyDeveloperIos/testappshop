//
//  Product+CoreDataProperties.m
//  
//
//  Created by Nazar Gorobets on 6/6/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

@dynamic countProduct;
@dynamic nameProduct;
@dynamic photoProduct;
@dynamic priceProduct;
@dynamic veganProduct;
@dynamic organicProduct;
@dynamic cathegory;

@end
