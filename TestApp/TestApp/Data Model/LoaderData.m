//
//  LoaderData.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/5/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "LoaderData.h"
#import "DataManager.h"


@implementation LoaderData

- (void) loadingDataForDB{
    
    self.itemImages = [@[@"1.jpg", @"2.jpg", @"3.jpg", @"4.jpg", @"5.jpg", @"6.jpg"] mutableCopy];
    self.itemName   = [@[@"банан" , @"яблуко", @"вишня", @"виноград", @"хз-шо", @"апельсин"]   mutableCopy];

    NSMutableArray* objectsArray = [NSMutableArray array];
    
    for (int i = 0; i < self.itemImages.count; i++) {
        
            int countProduct = 1;
        
            DataManager* mission = [[DataManager alloc] initWithServerSecurityResponse:[self.itemName objectAtIndex:i] namePhoto:[self.itemImages objectAtIndex:i] countProduct:countProduct setCathegory:@"Fruits"];
        
            [objectsArray addObject:mission];
    }
    
    self.itemImages = [@[@"images-1.jpg", @"images-2.jpg", @"images-3.jpg", @"images-4.jpg", @"images-5.jpg"] mutableCopy];
    self.itemName   = [@[@"суп-1" , @"суп-2", @"суп-3", @"суп-4", @"суп-5"]   mutableCopy];
    
    NSMutableArray* objectsArraySoup = [NSMutableArray array];
    
    for (int i = 0; i < self.itemImages.count; i++) {
        
        int countProduct = 1;
        
        DataManager* mission = [[DataManager alloc] initWithServerSecurityResponse:[self.itemName objectAtIndex:i] namePhoto:[self.itemImages objectAtIndex:i] countProduct:countProduct setCathegory:@"Food"];
        
        [objectsArraySoup addObject:mission];
    }
}

@end
