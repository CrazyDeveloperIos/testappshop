//
//  ViewController.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderView.h"

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, OrderViewDelegate>


@end

