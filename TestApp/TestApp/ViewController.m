//
//  ViewController.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "ViewController.h"
#import "CustomCollectionViewCell.h"
#import "MunuInGroupViewController.h"
#import "Order+CoreDataProperties.h"
#import "OrderView.h"
#import "OrderViewController.h"
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *menuImages;
@property (strong, nonatomic) NSArray *cathegory;
@property (strong, nonatomic) OrderView *orderView;

@property (nonatomic) NSMutableArray* orders;
@property (nonatomic) Order* order;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self loadViewAndData];
  
}

- (void) loadViewAndData{
    
    self.menuImages = [@[@"1.jpg",  @"tortilla-soup-2.jpg"] mutableCopy];
    self.cathegory  = [@[@"Fruits", @"Food"] copy];
    
    self.orders = [[Order MR_findAllSortedBy:@"price" ascending:YES] mutableCopy];
    
    double priceOrder;
    
    for (int i = 0; i < [self.orders count]; i++) {
        
        Order *order  = [self.orders objectAtIndex:i];
        priceOrder   += [order.price doubleValue];
        
    }
    if (priceOrder != 0) {
        
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView = [[OrderView alloc]
                                               loadOrderView:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50)
                                               addPrice:priceOrder];
                              self.orderView.delegate = self;
                             [self.view addSubview:self.orderView];
                             
                         }
                         completion:NULL];
    }
    
}

#pragma mark Collection View DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.menuImages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomCollectionViewCell *Cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell"  forIndexPath:indexPath];
    
    UIImage *image;
    long row = [indexPath row];
    
    image = [UIImage imageNamed:self.menuImages[row]];
    Cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    Cell.layer.borderWidth = 1;
    Cell.imageMenu.image = image;
    Cell.nameMenu.text = [self.cathegory objectAtIndex:indexPath.row];
    
    return Cell;
}

#pragma mark Collection view layout things

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    double sizeCell = collectionView.layer.frame.size.width / 2 - 20;
    
    CGSize mElementSize = CGSizeMake(sizeCell, sizeCell + 40);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 15.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 15.0;
}

#pragma mark Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    MunuInGroupViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"MunuInGroupViewController"];
    
    viewController.cathegory = [self.cathegory objectAtIndex:indexPath.row];
    [self showViewController:viewController sender:nil];

    
}

#pragma mark Order View Delegate

- (void) didTapButtonOrder:(OrderView*)view{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    OrderViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
    
    [self showViewController:viewController sender:nil];
    
}

@end
