//
//  OrderViewController.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/7/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
