//
//  MunuInGroupViewController.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "MunuInGroupViewController.h"
#import "CustomTableViewCell.h"
#import "OrderView.h"
#import "OrderViewController.h"
#import "Product+CoreDataProperties.h"
#import "Order+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>


@interface MunuInGroupViewController () <UITableViewDelegate, UITableViewDataSource, CustomTableViewCellDelegate, OrderViewDelegate>

@property (weak,   nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleViewController;

@property (strong, nonatomic) OrderView *orderView;

@property (nullable, nonatomic, retain) NSString *nameProduct;
@property (nullable, nonatomic, retain) NSData   *photoProduct;
@property (nullable, nonatomic, retain) NSNumber *countProduct;
@property (nullable, nonatomic, retain) NSNumber *priceProduct;
@property (nullable, nonatomic, retain) NSNumber *veganProduct;
@property (nullable, nonatomic, retain) NSNumber *organicProduct;

@property (nonatomic) NSMutableArray* products;
@property (nonatomic) Product* product;
@property (nonatomic) NSMutableArray* orders;
@property (nonatomic) Order* order;

@end

@implementation MunuInGroupViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.titleViewController.text = @"The use of text messaging for business purposes has grown significantly during the mid-2000s. As companies seek competitive advantages, many employees turn to new technology.";
    self.navigationItem.title = self.cathegory;
    [self loadViewAndData];
    

}

- (void) loadViewAndData{
    
        self.products = [[Product MR_findByAttribute:@"cathegory" withValue:self.cathegory] mutableCopy];
    
        self.orders = [[Order MR_findAllSortedBy:@"price" ascending:YES] mutableCopy];
    
    double priceOrder;
    
    for (int i = 0; i < [self.orders count]; i++) {
        
        Order *order  = [self.orders objectAtIndex:i];
        priceOrder   += [order.price doubleValue];

    }
    if (priceOrder != 0) {
        
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView = [[OrderView alloc]
                                               loadOrderView:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50)
                                               addPrice:priceOrder];
                              self.orderView.delegate = self;
                             [self.view addSubview:self.orderView];
                             
                         }
                         completion:NULL];
    }
    
}

- (void) removeEntyty:(id) name{
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        if ([name isKindOfClass:[Product class]]) {
            
            for (Product *safetyToRemove in self.products) {
                
                [safetyToRemove MR_deleteEntity];
            }
        }
        
        [self saveContext];
        
    } completion:^(BOOL success, NSError *error) {
        
        if(success) {
            
            NSLog(@"Entyty %@ Delete", name);
            
            self.product = nil;
            
        } else {
            
        }
        
    }];
    
}

- (void)saveContext {
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
}

#pragma mark Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
    
    for (int i = 0; i < self.products.count; i++) {
        
        Product *product  = [self.products objectAtIndex:indexPath.row];
        
        self.nameProduct    = product.nameProduct;
        self.photoProduct   = product.photoProduct;
        self.countProduct   = product.countProduct;
        self.priceProduct   = product.priceProduct;
        self.veganProduct   = product.veganProduct;
        self.organicProduct = product.organicProduct;
        
    }

     UIImage *imageProduct = [UIImage imageWithData:self.photoProduct];
    
     cell.tableView = self.tableView;
     cell.delegate  = self;
     cell.nameProduct.text = self.nameProduct;
     cell.imageProduct.image = imageProduct;
     cell.priceProduct.text  = [NSString stringWithFormat:@"%@ $", self.priceProduct];
    
     cell.countSelectedProduct = [self.countProduct integerValue];
    
     cell.countProductForSales.text = [NSString stringWithFormat:@"%@",self.countProduct];
    
    if ([self.veganProduct  isEqual: @0]) {
        
        cell.isVeganProduct.hidden = YES;
        
    }
    
    if ([self.organicProduct  isEqual: @0]) {
        
        cell.isOrganicProduct.hidden = YES;
    }

    return cell;
}

#pragma mark Custom TableViewCell Delegate

- (void) customCell:(CustomTableViewCell*)cell countProduct:(NSInteger) count didSelectButtonToOrderIndexPath:(NSIndexPath *)indexPath{
    
    [self addNewProductToOrder:count positionNumber:indexPath.row];
    
    
}

- (void) addNewProductToOrder:(NSInteger) count positionNumber:(NSInteger) number{
    
    Product *product  = [self.products objectAtIndex:number];
    
    Order *order = [Order MR_createEntity];
    
    double price = [product.priceProduct doubleValue];
    double priceCount = count;
    
    order.nameUser = @"User";
    order.name  = product.nameProduct;
    order.photo = product.photoProduct;
    order.count = [NSNumber numberWithInteger:count];
    order.price = [NSNumber numberWithDouble: price * priceCount];
    
    
    UIApplication *application   = [UIApplication sharedApplication];
    
    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
        
    }];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        
    } completion:^(BOOL success, NSError *error) {
        
        [application endBackgroundTask:bgTask];
         bgTask = UIBackgroundTaskInvalid;
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             [self.orderView removeFromSuperview];
                             [self loadViewAndData];
                             
                         }
                         completion:NULL];

        
    }];

    
}


- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    NSArray *visibleRows = [self.tableView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    
    NSIndexPath *path = [self.tableView indexPathForCell:lastVisibleCell];

    if(path.row == [self.products count] - 1){
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView.frame = CGRectMake(0, self.view.frame.size.height + 100, self.view.frame.size.width, 50);
                             
                         }
                         completion:NULL];
    }
    if(path.row == [self.products count] - 2){
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
                             
                         }
                         completion:NULL];
    }
}

#pragma mark Order View Delegate

- (void) didTapButtonOrder:(OrderView*)view{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    OrderViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
    
    [self showViewController:viewController sender:nil];
}


@end
