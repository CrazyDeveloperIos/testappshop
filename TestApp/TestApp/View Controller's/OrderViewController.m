//
//  OrderViewController.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/7/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "OrderViewController.h"
#import "Order+CoreDataProperties.h"
#import "OrderTableViewCell.h"
#import "OrderView.h"
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

@interface OrderViewController() <OrderViewDelegate>

@property (nullable, nonatomic, retain) NSString *nameUser;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSData *photo;
@property (nullable, nonatomic, retain) NSNumber *count;
@property (nullable, nonatomic, retain) NSNumber *price;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray* orders;
@property (nonatomic) Order* order;
@property (strong, nonatomic) OrderView *orderView;

@end

@implementation OrderViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
     self.orders = [[Order MR_findAllSortedBy:@"price" ascending:YES] mutableCopy];
    
    double priceOrder;
    
    for (int i = 0; i < [self.orders count]; i++) {
        
        Order *order  = [self.orders objectAtIndex:i];
        priceOrder   += [order.price doubleValue];
        
    }
    if (priceOrder != 0) {
        
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView = [[OrderView alloc]
                                               loadOrderView:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50)
                                               addPrice:priceOrder];
                              self.orderView.delegate = self;
                             [self.view addSubview:self.orderView];
                             
                         }
                         completion:NULL];
    }
    
}


#pragma mark Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderTableViewCell"];
        
    Order *order  = [self.orders objectAtIndex:indexPath.row];
        
        self.nameUser = order.nameUser;
        self.name     = order.name;
        self.photo    = order.photo;
        self.count    = order.count;
        self.price    = order.price;
        

    
    UIImage *imageProduct = [UIImage imageWithData:self.photo];
    
    double price = [self.price doubleValue];
    double priceCount = [self.count doubleValue];
    
    cell.nameProduct.text = self.name;
    cell.imageProduct.image = imageProduct;
    cell.priceOneProduct.text  = [NSString stringWithFormat:@"%@ $", self.price];
    cell.priceInContext.text = [NSString stringWithFormat:@"%.2f $", price * priceCount];
    cell.countProduct.text = [NSString stringWithFormat:@"%@",self.self.count];

    
    return cell;
    
}

#pragma mark Order View Delegate

- (void) didTapButtonOrder:(OrderView*)view{
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    NSArray *visibleRows = [self.tableView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    
    NSIndexPath *path = [self.tableView indexPathForCell:lastVisibleCell];
    
    if(path.row == [self.orders count] - 1){
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView.frame = CGRectMake(0, self.view.frame.size.height + 100, self.view.frame.size.width, 50);
                             
                         }
                         completion:NULL];
    }
    if(path.row == [self.orders count] - 2){
        [UIView animateWithDuration:0.6
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^(void) {
                             
                             self.orderView.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
                             
                         }
                         completion:NULL];
    }
}


@end
