//
//  OrderView.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/6/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "OrderView.h"
#import "OrderViewController.h"

@implementation OrderView

- (OrderView*) loadOrderView:(CGRect) frame addPrice:(double)price{
    
    OrderView* orderView = [[OrderView alloc] initWithFrame:frame];
    
    orderView.backgroundColor = [UIColor redColor];
    orderView.alpha = 0.7;
    
    UIImage* image = [UIImage imageNamed:@"logo.png"];
    UIView* viewImage = [[UIImageView alloc] initWithImage:image];
    viewImage.frame = CGRectMake(10, 12.5, 30, 30);
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 80, 30)];
    label.backgroundColor = [UIColor redColor];
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%.2f", price];
    
     UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 100, 10, 80, 30)];
    [button setTitle:@"Order" forState:UIControlStateNormal];
     button.layer.borderColor = [UIColor whiteColor].CGColor;
     button.layer.borderWidth = 1;
    [button addTarget:self
               action:@selector(showOrderViewController)
     forControlEvents:UIControlEventTouchUpInside];
    
    [orderView addSubview:viewImage];
    [orderView addSubview:button];
    [orderView addSubview:label];
    
    return orderView;
}

- (void) showOrderViewController{
 
    [self.delegate didTapButtonOrder:self];
    
}
@end
