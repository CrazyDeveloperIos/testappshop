//
//  OrderView.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/6/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderView;

@protocol OrderViewDelegate

@required

- (void) didTapButtonOrder:(OrderView*)view;

@end

@interface OrderView : UIView

@property (weak, nonatomic) id <OrderViewDelegate> delegate;

- (OrderView *)loadOrderView:(CGRect) frame addPrice:(double)price;


@end
