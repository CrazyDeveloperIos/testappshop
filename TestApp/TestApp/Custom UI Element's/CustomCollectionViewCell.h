//
//  CustomCollectionViewCell.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imageMenu;
@end
