//
//  CustomTableViewCell.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomTableViewCell;

@protocol CustomTableViewCellDelegate

@optional

- (void) customCell:(CustomTableViewCell*)cell countProduct:(NSInteger) count didSelectButtonToOrderIndexPath:(NSIndexPath *)indexPath;

@end

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) id <CustomTableViewCellDelegate> delegate;

@property (strong, nonatomic) UITableView* tableView;

@property (assign, nonatomic) NSInteger countSelectedProduct;
@property (weak, nonatomic) IBOutlet UIButton *addToOrderButton;

@property (weak, nonatomic) IBOutlet UILabel *nameProduct;
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UITextView *descriptionProduct;
@property (weak, nonatomic) IBOutlet UIView  *isOrganicProduct;
@property (weak, nonatomic) IBOutlet UIView  *isVeganProduct;
@property (weak, nonatomic) IBOutlet UILabel *priceProduct;
@property (weak, nonatomic) IBOutlet UIButton *removeCount;
@property (weak, nonatomic) IBOutlet UIButton *addCount;
@property (weak, nonatomic) IBOutlet UILabel  *countProductForSales;

@end
