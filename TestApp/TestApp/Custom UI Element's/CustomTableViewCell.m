//
//  CustomTableViewCell.m
//  TestApp
//
//  Created by Nazar Gorobets on 6/4/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import "CustomTableViewCell.h"

@interface CustomTableViewCell ()

@end

@implementation CustomTableViewCell

- (void) awakeFromNib {
    
    [super awakeFromNib];

     self.addToOrderButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
     self.addToOrderButton.layer.borderWidth = 1;
}

- (IBAction)addToOrderButton:(id)sender {
    
     NSIndexPath * indexPath = [self.tableView indexPathForCell:self];
     [self.delegate   customCell:self
                    countProduct:self.countSelectedProduct
didSelectButtonToOrderIndexPath:indexPath];

}
- (IBAction)addOneButton:(id)sender {
    
    self.countSelectedProduct +=1;
    self.countProductForSales.text = [NSString stringWithFormat:@"%li", self.countSelectedProduct];
    
}
- (IBAction)removeOneButton:(id)sender {
    if (self.countSelectedProduct > 1) {
        self.countSelectedProduct -= 1;
        self.countProductForSales.text = [NSString stringWithFormat:@"%li", self.countSelectedProduct];
    }
}

@end
