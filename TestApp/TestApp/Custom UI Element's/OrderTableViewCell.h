//
//  OrderTableViewCell.h
//  TestApp
//
//  Created by Nazar Gorobets on 6/7/16.
//  Copyright © 2016 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *nameProduct;
@property (weak, nonatomic) IBOutlet UILabel *priceOneProduct;
@property (weak, nonatomic) IBOutlet UILabel *priceInContext;
@property (weak, nonatomic) IBOutlet UIButton *removeProduct;
@property (weak, nonatomic) IBOutlet UILabel *countProduct;

@end
